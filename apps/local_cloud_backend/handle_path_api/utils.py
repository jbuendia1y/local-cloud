import os
from pathlib import Path

def get_tree_from_dir(path:str=""):
    work_dir = Path("static",path)

    tree = os.listdir(work_dir)
    return tree

def upload_file(path:str,filename,data):
    file_path = Path(path,filename)
    os.write(file_path,data)

def create_folder(path:str):
    os.mkdir(path)