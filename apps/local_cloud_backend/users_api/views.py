from django.shortcuts import render
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from django.contrib.auth import get_user_model,login

from users_api.serializers import UserLoginSerializer, UserModelSerializer

# Create your views here.

def login(request:Request):
    data = request.data

    serializer = UserLoginSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    user,token = serializer.save()

    login(request,user)

    return {
        "user": UserModelSerializer(user).data,
        "token":token
    }

class UserProfileApiView(APIView):
    def get(self,request:Request):
        users = get_user_model().objects.all()
        return Response(data=users.values(),status=status.HTTP_200_OK)