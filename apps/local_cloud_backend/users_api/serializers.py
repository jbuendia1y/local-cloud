from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token

from users_api.models import UserProfile

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = (
            "document",
            "first_name",
            "last_name",
            "is_superuser",
            "is_staff",
            "avatar",
            "phone",
            "role"
        )

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self,data):
        user =  authenticate(
            email=data["email"],
            password=data["password"]
        )

        if not user:
            raise serializers.ValidationError("The credentials are not valid")
        
        self.context["user"] = user
        return data

    def create(self,data):
        token,created = Token.objects.get_or_create(
            user=self.context.get("user")
        )

        return self.context.get("user"),token.key