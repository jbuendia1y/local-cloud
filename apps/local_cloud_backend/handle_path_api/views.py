from django.shortcuts import render
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from handle_path_api.utils import get_tree_from_dir


# Create your views here.

class HandleRootApi(APIView):
    def get(self,request:Request):
        root_dir = get_tree_from_dir()
        return Response(root_dir,status=status.HTTP_200_OK)

class HandlePathApi(APIView):

    def get(self,request:Request,path:str=None):
        try:
            root_dir = get_tree_from_dir(path)
        except FileNotFoundError as e:
            raise FileNotFoundError(f"The path: {path}, cannot found in the system")
        return Response(root_dir,status=status.HTTP_200_OK)

