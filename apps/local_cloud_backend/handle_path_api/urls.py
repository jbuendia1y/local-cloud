from django.urls import path
from handle_path_api import views


urlpatterns = [
    path("",views.HandleRootApi().as_view()),
    path("<path:path>",views.HandlePathApi().as_view())
]