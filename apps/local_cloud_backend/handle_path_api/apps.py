from django.apps import AppConfig


class HandlePathApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'handle_path_api'
