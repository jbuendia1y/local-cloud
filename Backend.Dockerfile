FROM python:3

WORKDIR /app

# INSTALL PIPENV
RUN pip install pipenv

# COPY AND INSTALL REQUIREMENTS

COPY ./apps/local_cloud_backend/Pipfile .
COPY ./apps/local_cloud_backend/Pipfile.lock .
RUN pipenv install

# COPY ALL BACKEND FILES
COPY ./apps/local_cloud_backend .

# EXECUTE PYTHON MANAGE.PY
CMD ["pipenv","run","serve"]
